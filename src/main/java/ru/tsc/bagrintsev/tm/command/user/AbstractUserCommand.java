package ru.tsc.bagrintsev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.IUserService;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ServiceLocatorNotInitializedException;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserService getUserService() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getUserService();
    }

    @NotNull
    public IAuthService getAuthService() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator.getAuthService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[0];
    }

    @NotNull
    @Override
    public String getShortName() {
        return "";
    }

}
