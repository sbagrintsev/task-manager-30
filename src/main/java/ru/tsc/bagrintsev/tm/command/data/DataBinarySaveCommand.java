package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        try (@NotNull final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file, false))) {
            oos.writeObject(domain);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in binary file";
    }

}
