package ru.tsc.bagrintsev.tm.command.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class AboutCommand extends AbstractSystemCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        System.out.printf("Author name: %s%n", getPropertyService().getAuthorName());
        System.out.printf("E-mail: %s%n", getPropertyService().getAuthorEmail());
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print about author.";
    }

}
