package ru.tsc.bagrintsev.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataJacksonXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-xml";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_JACKSON_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from xml file";
    }

}
