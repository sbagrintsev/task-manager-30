package ru.tsc.bagrintsev.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.data.AbstractDataCommand;
import ru.tsc.bagrintsev.tm.command.data.BackupLoadCommand;
import ru.tsc.bagrintsev.tm.command.data.BackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    private void load() {
        if (Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) {
            bootstrap.processOnTheGo(BackupLoadCommand.NAME, false);
        }
    }

    protected void save() {
        bootstrap.processOnTheGo(BackupSaveCommand.NAME, false);
    }

}
