package ru.tsc.bagrintsev.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File folder = new File(".");

    @NotNull
    private final Bootstrap bootstrap;

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getShortCommands();
        commands.forEach(c -> this.commands.add(c.getName()));
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    public void start() {
        init();
    }

    public void stop() {
        es.shutdown();
    }

    @SneakyThrows
    private void process() {
        for (File file : Objects.requireNonNull(folder.listFiles())) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            if (commands.contains(fileName)) {
                try {
                    file.delete();
                    bootstrap.processOnTheGo(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
